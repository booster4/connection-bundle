# ConnectionBundle

## Configuration example
```yaml
# config/services.yaml
services:
    ...
    test_controller:
        class: ConnectionBundle\Interceptor\HttpInterceptor
        public: true
        arguments:
            $extractors:
                - '@ConnectionBundle\RequestDataExtractor\GetParametersExtractor'
                - '@ConnectionBundle\RequestDataExtractor\PathParametersExtractor'
            $handler: '@App\Context\Test\Service\TestService'
            $requestClass: 'App\Context\Test\Service\TestRequest'
```

```yaml
# config/routes.yaml
test_route:
    path: '/test/{page<\d+>}'
    methods: ['GET']
    controller: test_controller
    requirements:
        page: '\d+'
```

```php
<?php
# TestRequest.php
namespace App\Context\Test\Service;

use Booster\ConnectionBundle\DTO\RequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class TestRequest implements RequestInterface
{
    #[Groups("path")]
    #[Assert\Positive]
    private int $page;

    #[Groups("get")]
    #[Assert\NotBlank]
    private string $qwe;

    #[Groups("get")]
    #[Assert\NotBlank]
    private string $foo;

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): TestRequest
    {
        $this->page = $page;

        return $this;
    }

    public function getQwe(): string
    {
        return $this->qwe;
    }

    public function setQwe(string $qwe): TestRequest
    {
        $this->qwe = $qwe;

        return $this;
    }

    public function getFoo(): string
    {
        return $this->foo;
    }

    public function setFoo(string $foo): TestRequest
    {
        $this->foo = $foo;

        return $this;
    }
}
```

```php
<?php
# TestResponse.php
namespace App\Context\Test\Service;

use Booster\ConnectionBundle\DTO\BaseSuccessfulResponse;
use Symfony\Component\Validator\Constraints as Assert;

class TestResponse extends BaseSuccessfulResponse
{
    #[Assert\NotBlank]
    #[Assert\Length(min: 3)]
    public readonly string $name;

    #[Assert\All([
        new Assert\NotBlank,
        new Assert\Length(min: 5),
    ])]
    public readonly array $names;

    public function __construct(string $name, array $names)
    {
        $this->name = $name;
        $this->names = $names;
    }
}
```

```php
<?php
# TestService.php
namespace App\Context\Card\Service;

use Booster\ConnectionBundle\DTO\RequestInterface;
use Booster\ConnectionBundle\DTO\ResponseInterface;
use Booster\ConnectionBundle\Handler\HandlerInterface;

class TestService implements HandlerInterface
{
    public function handle(RequestInterface $request): ResponseInterface
    {
        return new TestResponse('my name', ['other name', 'and one more another name']);
    }
}
```
