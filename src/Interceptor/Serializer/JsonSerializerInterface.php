<?php

namespace Booster\ConnectionBundle\Interceptor\Serializer;

interface JsonSerializerInterface
{
    public function serialize(mixed $data): string;
}
