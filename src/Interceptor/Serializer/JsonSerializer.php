<?php

namespace Booster\ConnectionBundle\Interceptor\Serializer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class JsonSerializer implements JsonSerializerInterface
{
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    public function serialize(mixed $data): string
    {
        return $this->serializer->serialize($data, JsonEncoder::FORMAT);
    }
}
