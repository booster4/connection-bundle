<?php

namespace Booster\ConnectionBundle\Interceptor;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface InterceptorInterface
{
    public function __invoke(Request $request): Response;
}
