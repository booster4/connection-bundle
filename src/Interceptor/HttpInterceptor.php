<?php

namespace Booster\ConnectionBundle\Interceptor;

use Booster\ConnectionBundle\DTO\ExceptionResponse;
use Booster\ConnectionBundle\DTO\RequestInterface;
use Booster\ConnectionBundle\Handler\HandlerInterface;
use Booster\ConnectionBundle\Interceptor\Normalizer\ArrayDenormalizer;
use Booster\ConnectionBundle\Interceptor\Serializer\JsonSerializerInterface;
use Booster\ConnectionBundle\Interceptor\Validator\ObjectValidatorInterface;
use Booster\ConnectionBundle\RequestDataExtractor\ParametersExtractorInterface;
use Booster\ConnectionBundle\Transformer\ViolationsListToArray;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class HttpInterceptor implements InterceptorInterface
{
    /**
     * @param ParametersExtractorInterface[] $extractors
     */
    public function __construct(
        private readonly array $extractors,
        private readonly HandlerInterface $handler,
        private readonly string $requestClass,
        private readonly JsonSerializerInterface $serializer,
        private readonly ObjectValidatorInterface $validator,
        private readonly ViolationsListToArray $violationsListToArray,
        private readonly ArrayDenormalizer $denormalizer
    ) {
    }

    public function __invoke(Request $request): Response
    {
        try {
            $requestDTO = $this->convertRequestToRequestDTO($request);
        } catch (ExceptionInterface $e) {
            $response = new ExceptionResponse($e);
            $errorResponse = $this->serializer->serialize($response);

            return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST, [], true);
        }

        $violations = $this->validator->validate($requestDTO);
        if ($violations->count() > 0) {
            $errorResponseDTO = $this->violationsListToArray->convert($violations);
            $errorResponse = $this->serializer->serialize($errorResponseDTO);

            return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST, [], true);
        }

        $responseDTO = $this->handler->handle($requestDTO);
        if ($responseDTO->isSuccessful()) {
            $violations = $this->validator->validate($responseDTO);

            if ($violations->count() > 0) {
                $errorResponseDTO = $this->violationsListToArray->convert($violations);
                $errorResponse = $this->serializer->serialize($errorResponseDTO);

                return new JsonResponse($errorResponse, Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
            }

            $response = $this->serializer->serialize($responseDTO);

            return new JsonResponse($response, Response::HTTP_OK, [], true);
        }

        $response = $this->serializer->serialize($responseDTO);

        return new JsonResponse($response, Response::HTTP_BAD_REQUEST, [], true);
    }

    /**
     * @param Request $request
     * @return RequestInterface
     * @throws ExceptionInterface
     */
    private function convertRequestToRequestDTO(Request $request): RequestInterface
    {
        $requestDto = new $this->requestClass;

        foreach ($this->extractors as $extractor) {
            $this->denormalizer->denormalize(
                $extractor->extract($request),
                $this->requestClass,
                $extractor->getParametersGroup(),
                $requestDto,
            );
        }

        return $requestDto;
    }
}

