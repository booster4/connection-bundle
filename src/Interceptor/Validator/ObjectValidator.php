<?php

namespace Booster\ConnectionBundle\Interceptor\Validator;

use Booster\ConnectionBundle\DTO\RequestInterface;
use Booster\ConnectionBundle\DTO\ResponseInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ObjectValidator implements ObjectValidatorInterface
{
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    public function validate(RequestInterface|ResponseInterface $object): ConstraintViolationListInterface
    {
        return $this->validator->validate($object);
    }
}
