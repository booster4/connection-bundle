<?php

namespace Booster\ConnectionBundle\Interceptor\Validator;

use Booster\ConnectionBundle\DTO\RequestInterface;
use Booster\ConnectionBundle\DTO\ResponseInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ObjectValidatorInterface
{
    public function validate(RequestInterface|ResponseInterface $object): ConstraintViolationListInterface;
}
