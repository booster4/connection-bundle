<?php

namespace Booster\ConnectionBundle\Interceptor\Normalizer;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ArrayDenormalizer implements ArrayDenormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param array $data
     * @param string $class
     * @param string[] $groups
     * @param object|null $object
     * @return object
     * @throws ExceptionInterface
     */
    public function denormalize(array $data, string $class, array $groups = [], object $object = null): object
    {
        return $this->normalizer->denormalize(
            $data,
            $class,
            'array',
            $this->provideDeserializeOptions($groups, $object)
        );
    }

    /**
     * @param string[] $groups
     * @param object|null $object
     * @return array
     */
    private function provideDeserializeOptions(array $groups = [], object $object = null): array
    {
        $options = [
            'groups' => $groups,
            'disable_type_enforcement' => true,
        ];

        if ($object !== null) {
            $options['object_to_populate'] = $object;
        }

        return $options;
    }
}
