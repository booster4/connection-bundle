<?php

namespace Booster\ConnectionBundle\Interceptor\Normalizer;

interface ArrayDenormalizerInterface
{
    public function denormalize(array $data, string $class, array $groups, object $object): object;
}
