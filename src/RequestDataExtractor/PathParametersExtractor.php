<?php

namespace Booster\ConnectionBundle\RequestDataExtractor;

use Symfony\Component\HttpFoundation\Request;

class PathParametersExtractor implements ParametersExtractorInterface
{
    function extract(Request $request): array
    {
        return $request->attributes->get('_route_params') ?? [];
    }

    function getParametersGroup(): array
    {
        return ['path'];
    }
}
