<?php

namespace Booster\ConnectionBundle\RequestDataExtractor;

use Symfony\Component\HttpFoundation\Request;

class GetParametersExtractor implements ParametersExtractorInterface
{
    function extract(Request $request): array
    {
        return iterator_to_array($request->query->getIterator());
    }

    function getParametersGroup(): array
    {
        return ['get'];
    }
}
