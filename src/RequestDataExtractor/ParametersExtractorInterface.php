<?php

namespace Booster\ConnectionBundle\RequestDataExtractor;

use Symfony\Component\HttpFoundation\Request;

interface ParametersExtractorInterface
{
    function extract(Request $request): array;

    /**
     * @return string[]
     */
    function getParametersGroup(): array;
}
