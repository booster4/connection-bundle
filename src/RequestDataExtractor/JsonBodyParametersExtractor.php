<?php

namespace Booster\ConnectionBundle\RequestDataExtractor;

use Symfony\Component\HttpFoundation\Request;

class JsonBodyParametersExtractor implements ParametersExtractorInterface
{
    function extract(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }

    function getParametersGroup(): array
    {
        return ['json'];
    }
}
