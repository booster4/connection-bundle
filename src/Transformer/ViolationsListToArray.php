<?php

namespace Booster\ConnectionBundle\Transformer;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ViolationsListToArray
{
    public function convert(ConstraintViolationListInterface $violations): array
    {
        $errors = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $path = explode('.', $violation->getPropertyPath());

            $root = &$errors;
            foreach ($path as $value) {
                $root[$value] = $root[$value] ?? [];
                $root = &$root[$value];
            }
            $root = array_merge($root, [$violation->getMessage()]);
            unset($root);
        }

        return $errors;
    }
}
