<?php

namespace Booster\ConnectionBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ConnectionExtension extends Extension
{
    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $location = new FileLocator(__DIR__ . '/../Resources/config');
        $loader = new YamlFileLoader($container, $location);

        $loader->load('services.yaml');
    }
}
