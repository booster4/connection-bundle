<?php

namespace Booster\ConnectionBundle\DTO;

interface ResponseInterface
{
    public function isSuccessful(): bool;

    public function isError(): bool;
}
