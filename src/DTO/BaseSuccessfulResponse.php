<?php

namespace Booster\ConnectionBundle\DTO;

use Symfony\Component\Serializer\Annotation\Ignore;

abstract class BaseSuccessfulResponse implements ResponseInterface
{
    #[Ignore]
    final public function isSuccessful(): bool
    {
        return true;
    }

    #[Ignore]
    final public function isError(): bool
    {
        return false;
    }
}
