<?php

namespace Booster\ConnectionBundle\DTO;

final class ErrorResponse extends BaseErrorResponse
{
    public function __construct(private readonly string $error)
    {
    }

    public function getError(): string
    {
        return $this->error;
    }
}
