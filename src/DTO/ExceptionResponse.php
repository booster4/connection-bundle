<?php

namespace Booster\ConnectionBundle\DTO;

use Throwable;

final class ExceptionResponse extends BaseErrorResponse
{
    public function __construct(private readonly Throwable $exception)
    {
    }

    public function getException(): string
    {
        return $this->exception->getMessage();
    }
}
