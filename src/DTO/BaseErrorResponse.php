<?php

namespace Booster\ConnectionBundle\DTO;

use Symfony\Component\Serializer\Annotation\Ignore;

abstract class BaseErrorResponse implements ResponseInterface
{
    #[Ignore]
    final public function isSuccessful(): bool
    {
        return false;
    }

    #[Ignore]
    final public function isError(): bool
    {
        return true;
    }
}
