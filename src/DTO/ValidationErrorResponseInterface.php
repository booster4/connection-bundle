<?php

namespace Booster\ConnectionBundle\DTO;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ValidationErrorResponseInterface
{
    public function getErrors(): ConstraintViolationListInterface;
}
