<?php

namespace Booster\ConnectionBundle\DTO;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ValidationErrorResponse extends BaseErrorResponse implements ValidationErrorResponseInterface
{
    public function __construct(private readonly ConstraintViolationListInterface $violationList)
    {
    }

    public function getErrors(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
