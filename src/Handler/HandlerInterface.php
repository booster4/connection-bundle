<?php

namespace Booster\ConnectionBundle\Handler;

use Booster\ConnectionBundle\DTO\RequestInterface;
use Booster\ConnectionBundle\DTO\ResponseInterface;

interface HandlerInterface
{
    public function handle(RequestInterface $request): ResponseInterface;
}
